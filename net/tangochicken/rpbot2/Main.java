package net.tangochicken.rpbot2;

import org.pircbotx.Configuration;
import org.pircbotx.PircBotX;
import org.pircbotx.cap.EnableCapHandler;

public class Main {

	public static void main(String[] args) {
		NetworkHandler.updateVariables();
		BotTick tick = new BotTick();
		tick.start();
		Configuration configuration = new Configuration.Builder().setName(Data.USERNAME)
				.setServerPassword(Data.OAUTH).setAutoNickChange(false)
				.setOnJoinWhoEnabled(false).setCapEnabled(true)
				.addCapHandler(new EnableCapHandler("twitch.tv/membership"))
				.addCapHandler((new EnableCapHandler("twitch.tv/tags"))).addListener(new CommandListener())
				.addServer("irc.twitch.tv", 6667).addAutoJoinChannel(Data.CHANNEL).buildConfiguration();
		@SuppressWarnings("resource")
		PircBotX bot = new PircBotX(configuration);
		try {
			bot.startBot();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}