package net.tangochicken.rpbot2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import org.json.JSONObject;

class Request implements Callable<Response> {
	private URL url;

	public Request(URL url) {
		this.url = url;
	}

	@Override
	public Response call() throws Exception {
		URLConnection uc = url.openConnection();
		uc.setRequestProperty("Authorization", "Basic " + Data.APIAUTHTOKEN);
		return new Response(uc.getInputStream());
	}
}

class Response {
	private InputStream body;

	public Response(InputStream body) {
		this.body = body;
	}

	public InputStream getBody() {
		return body;
	}
}

public class NetworkHandler {
	
	public static void updateVariables(){
		Data.clientLastUpdated = System.currentTimeMillis() / 1000L;
		Data.commandObject = new JSONObject(getCommands());
		Data.quoteObject = new JSONObject(getQuotes());
	}

	public static long serverLastUpdated() {
		ExecutorService executor = Executors.newFixedThreadPool(1);
		String s = "0";
		try {
			FutureTask<Response> response = (FutureTask<Response>) executor
					.submit(new Request(new URL("http://api.weareracpack.com/rpbot/lastEdited")));
			InputStream body = response.get().getBody();
			InputStreamReader isReader = new InputStreamReader(body);
			BufferedReader reader = new BufferedReader(isReader);
			s = reader.readLine();
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		executor.shutdown();
		try {
			long l = Long.parseLong(s);
			return l;
		} catch (NumberFormatException e) {
		}
		return 0;
	}
	
	public static String getCommands() {
		ExecutorService executor = Executors.newFixedThreadPool(1);
		String s = null;
		try {
			FutureTask<Response> response = (FutureTask<Response>) executor
					.submit(new Request(new URL("http://api.weareracpack.com/rpbot/getCommands")));
			InputStream body = response.get().getBody();
			InputStreamReader isReader = new InputStreamReader(body);
			BufferedReader reader = new BufferedReader(isReader);
			s = reader.readLine();
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		executor.shutdown();
		return s;
	}
	
	public static String getQuotes() {
		ExecutorService executor = Executors.newFixedThreadPool(1);
		String s = null;
		try {
			FutureTask<Response> response = (FutureTask<Response>) executor
					.submit(new Request(new URL("http://api.weareracpack.com/rpbot/getQuotes")));
			InputStream body = response.get().getBody();
			InputStreamReader isReader = new InputStreamReader(body);
			BufferedReader reader = new BufferedReader(isReader);
			s = reader.readLine();
		} catch (InterruptedException | ExecutionException | IOException e) {
			e.printStackTrace();
		}
		executor.shutdown();
		return s;
	}

}
