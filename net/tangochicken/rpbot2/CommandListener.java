package net.tangochicken.rpbot2;

import java.util.HashMap;
import java.util.Random;

import org.json.JSONArray;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;

public class CommandListener extends ListenerAdapter {

	@Override
	public void onMessage(MessageEvent event) {
		if (event.getUser().getLogin().equalsIgnoreCase(Data.USERNAME))
			return;
		String msg = event.getMessage();
		if (!msg.startsWith("!"))
			return;
		String[] args = msg.split(" ");
		PermissionManager m = new PermissionManager(event.getV3Tags());
		JSONArray commands = Data.commandObject.getJSONArray("userCommands");
		JSONArray quotes = Data.quoteObject.getJSONArray("Quotes");
		for (int i = 0; i < commands.length(); i++) {
			if (commands.getJSONObject(i).getInt("e") == 1) {
				String command = commands.getJSONObject(i).getString("c");
				if (command.split(" ")[0].equalsIgnoreCase(args[0])) {
					if (m.getLevel() >= commands.getJSONObject(i).getInt("p")) {
						if ((Data.gCommands.containsKey(commands.getJSONObject(i).getInt("id"))
								&& (System.currentTimeMillis() / 1000L)
										- (commands.getJSONObject(i).getInt("g") * 60) >= Data.gCommands
												.get(commands.getJSONObject(i).getInt("id")))
								|| !Data.gCommands.containsKey(commands.getJSONObject(i).getInt("id"))) {
							if ((Data.uCommands.containsKey(event.getUser().getLogin())
									&& Data.uCommands.get(event.getUser().getLogin())
											.containsKey(commands.getJSONObject(i).getInt("id"))
									&& (System.currentTimeMillis() / 1000L)
											- (commands.getJSONObject(i).getInt("u") * 60) >= Data.uCommands
													.get(event.getUser().getLogin())
													.get(commands.getJSONObject(i).getInt("id")))
									|| Data.uCommands.containsKey(event.getUser().getLogin())
											&& !Data.uCommands.get(event.getUser().getLogin())
													.containsKey(commands.getJSONObject(i).getInt("id"))
									|| !Data.uCommands.containsKey(event.getUser().getLogin())) {
								switch (args[0]) {
								case "!quote":
									int r = new Random().nextInt(quotes.length());
									event.respondChannel("Quote #" + quotes.getJSONObject(r).getString("id") + ": \""
											+ quotes.getJSONObject(r).getString("q") + "\" in "
											+ quotes.getJSONObject(r).getString("g") + ".");
									break;
								default:
									event.respondChannel(commands.getJSONObject(i).getString("r"));
									break;
								}
								Data.gCommands.put(commands.getJSONObject(i).getInt("id"),
										System.currentTimeMillis() / 1000L);
								HashMap<Integer, Long> newMap = new HashMap<Integer, Long>();
								if (Data.uCommands.containsKey(event.getUser().getLogin()))
									newMap = Data.uCommands.get(event.getUser().getLogin());
								newMap.put(commands.getJSONObject(i).getInt("id"), System.currentTimeMillis() / 1000L);
								Data.uCommands.put(event.getUser().getLogin(), newMap);
							}
						}
					}
					break;
				}
			}
		}
	}
}
