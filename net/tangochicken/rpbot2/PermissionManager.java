package net.tangochicken.rpbot2;

import com.google.common.collect.ImmutableMap;

public class PermissionManager {

	boolean mod = false;
	boolean subscriber = false;
	boolean turbo = false;
	boolean broadcaster = false;

	public PermissionManager(ImmutableMap<String, String> tags) {
		if (tags.containsKey("mod") && tags.get("mod").equals("1"))
			mod = true;
		if (tags.containsKey("subscriber") && tags.get("subscriber").equals("1"))
			subscriber = true;
		if (tags.containsKey("turbo") && tags.get("turbo").equals("1"))
			turbo = true;
		if (tags.containsKey("badges") && tags.get("badges").contains("broadcaster"))
			broadcaster = true;
	}

	public boolean isMod() {
		return mod;
	}

	public boolean isSub() {
		return subscriber;
	}

	public boolean isTurbo() {
		return turbo;
	}

	public boolean isCaster() {
		return broadcaster;
	}

	public int getLevel() {
		if (broadcaster)
			return 3;
		if (mod)
			return 2;
		if (subscriber)
			return 1;
		return 0;
	}

}
