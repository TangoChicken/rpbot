package net.tangochicken.rpbot2;

public class BotTick extends Thread {

	public void run() {
		long s = 1000;
		long i = 0;
		while (true) {
			i++;
			try {
				Thread.sleep(s);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(i%10 == 0){
				if(NetworkHandler.serverLastUpdated() > Data.clientLastUpdated){
					NetworkHandler.updateVariables();
				}
			}
		}
	}
}
